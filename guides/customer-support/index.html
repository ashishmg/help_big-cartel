<!doctype html>
<html lang="en">
  
<head>
    <title>Customer Support - Big Cartel Help</title>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  
    <link href="../../css/all.css" rel="stylesheet" type="text/css" />
    <link rel="canonical" href="index.html">
  </head>

  <body class="guide" style="background: #f7cf61 url(../../images/guides/customer-support/background-d82a9219.jpg) center 0 fixed no-repeat;">

    <div class="app">
      <div class="north-wrapper header--fixed">
        <header class="north">
          <div class="wrapper">
                <a href="../../index.html" class="logo mark">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 62 78" enable-background="new 0 0 62 78" xml:space="preserve">
    <g class="mark"><path d="M30.8 42.5l0-3c0-2 1-4 2.8-5.2l25.1-16c1.8-1.2 2.8-3.1 2.8-5.1V0L30.8 19.6L0 0v41.4C0 52.7 5 62 13.2 67.2l17.5 11.2 l17.5-11.2c8.2-5.2 13.2-14.4 13.2-25.8V22.9L30.8 42.5z"/>
    </g>
  </svg>
</a>
                <ul class="breadcrumbs">
                    <li> <a href="../index.html"><span>Back to </span>Guides</a></li>
                </ul>
            <a href="../../contact/index.html" class="contact">
              <span class="text">Contact Us</span>
              <span class="icon">
                <img width="15" height="auto" alt="Contact Us" src="../../images/layout/svg/envelope-icon.svg" />
              </span>
            </a>
          </div>
        </header>
      </div>

      <div class="content">
        
  <div class="support-main">
    <div class="support-body">
      <ul class="article-subnav">
        <li class="active"><a href="#intro">Intro</a></li>
        <li><a href="#get-the-heavy-lifting-out-of-the-way">Get the heavy lifting out of the way</a></li>
        <li><a href="#respond-quickly">Respond quickly</a></li>
        <li><a href="#ask-the-right-questions">Ask the right questions</a></li>
        <li><a href="#put-yourself-in-your-customers-shoes">Put yourself in your customer’s shoes</a></li>
        <li><a href="#the-golden-rule">The Golden Rule</a></li>
      </ul>

      <article id="intro">
        <div class="article-content">
          <h1 class="title">Customer Support</h1>
          <div>
              <p>Customer experience is one of the most important parts of any business, and it begins before you ever make a sale. If you’re just getting started, bookmark this page for later, get your pencil and paper ready, and study up on the basics of <a href="../customer-care/index.html">Customer Care</a>.</p>

<p>If you’re a seasoned pro in search of additional ideas, tips, and even insight into how we handle support at Big Cartel, then we’ve got what you’re looking for right here.</p>

<h3 id="get-the-heavy-lifting-out-of-the-way">Get the heavy lifting out of the way</h3>

<p>The term “canned response” has a pretty nasty connotation, and rightfully so. It’s like canned ham - made for the masses, but not actually good for any one person. But as an online store owner, you need to be prepared to respond to a lot of messages. Each support member on our team responds to hundreds of messages each week.</p>

<p><img alt="Illustration of canned ham" src="../../images/guides/customer-support/ham-3a50fc71.png" /></p>

<p>Inevitably, there will be a lot of overlap in the questions you’ll receive from your customers. Typing up the same list of steps more than a few times will start to cramp those little fingers, which might lead to you letting your email sit around a little longer than it should. It piles up quickly. Then you’re buried.</p>

<p>So, what do you do?</p>

<p>To avoid getting stuck in an endless cycle of being behind on your email, write up useful information in advance! You can create a Frequently Asked Questions (FAQ) page on your site that has all the details in one handy place. Many of your customers will now find their answer before they even think about writing an email.</p>

<p>When you do need to reply to emails, use a tool like <a href="http://smilesoftware.com/TextExpander/index.html">TextExpander</a> or <a href="https://www.trankynam.com/atext/">aText</a> to save common responses and have them ready to go with a few keystrokes.</p>

<p>Be careful with this! Use these tools to make yourself more efficient, not more lazy.</p>

<ul>
  <li><em>DON’T:</em> Write up the entire email with a bunch of generic junk. “Thank you for your feedback valued customer,” helps no one.</li>
  <li><em>DO:</em> Take the time to address your customer by name and tailor the response to their question and tone. “Hey Marcy” sounds a lot nicer than “Valued Customer” - doesn’t it?</li>
</ul>

<h3 id="respond-quickly">Respond quickly</h3>

<p><img alt="Illustration of fast emails" src="../../images/guides/customer-support/fast-ceb109f1.png" /></p>

<p>Don’t you hate it when you send an email and never hear back? Everyone else does, too.</p>

<p>Most customers expect a response within 24 hours. After that, you’ll see a pretty steep drop in customer satisfaction. At Big Cartel, we do our best to respond within a couple hours during the week, often even sooner than that.</p>

<p>Make sure you set the proper expectations. If you’re a one-person show, set up an email autoresponder letting your customers know you’ve received their email, and how soon they should expect a reply. Something as simple as, “You should hear back from me within 24 hours,” puts most people at ease.</p>

<p>When new messages are coming in fast, stay cool! Most customers would rather wait an hour for an exceptional answer instead of a hastily typed response in five minutes.</p>

<h3 id="ask-the-right-questions">Ask the right questions</h3>

<p>You made it! You’re ready to reply, and then you realize you don’t understand the question.</p>

<p>It’s difficult to communicate well through writing. Take an extra minute to ask some open-ended questions so you can fully and properly address their concerns. For example:</p>

<ul>
  <li><em>Can you send a picture so I can take a closer look?</em></li>
  <li><em>How often does this happen?</em></li>
  <li><em>How can I help make this right?</em></li>
</ul>

<p>Always assume the issue is real. You may just need to work together to solve it.</p>

<h3 id="put-yourself-in-your-customers-shoes">Put yourself in your customer’s shoes</h3>

<p><img alt="Illustration of a happy customer" src="../../images/guides/customer-support/happy-89af286f.png" /></p>

<p>Pop quiz! Answer the following questions:</p>

<ul>
  <li>What do you expect from great customer service?</li>
  <li>What experiences stand out to you?</li>
  <li>Isn’t it great when stores give you a refund without any hassle?</li>
  <li>Aren’t you more confident purchasing from a shop that has a generous return policy?</li>
</ul>

<p>Whatever your answers to those questions, start doing those things today!</p>

<p>When in doubt, do the right thing. That may mean refunding a purchase past your return window, or covering the shipping cost because your customer bought the wrong size.</p>

<p>The short-term hit is often worth the long-term gain.</p>

<p>Maybe it helps to think of it as a marketing expense. A common business saying goes, “A happy customer will tell one person. An unhappy customer will tell ten people.” Would you rather keep a few bucks now or attract more customers later?</p>

<h3 id="the-golden-rule">The Golden Rule</h3>

<p>Treat others how you want to be treated. That’s what it’s all about. Sometimes it’s tough to cover the extra expense of going above and beyond. We’ve all been there as starving artists, freelancers, or running our own businesses. Trust us when we say building a trustworthy name will take you to greater heights than any one transaction might. It’s about consistently doing what’s right, fair, and helpful to others.</p>


          </div>
        </div>
      </article>
    </div>
  </div>

      </div>

      <footer>
        <ul>
          <li>&copy; 2015 <a href="#">Big Cartel, LLC</a></li>
          <li><a href="#">Legal</a></li>
        </ul>
      </footer>
    </div>

    <script src="../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../js/all.js" type="text/javascript"></script>


  </body>

</html>
