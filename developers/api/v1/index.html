<!doctype html>
<html lang="en">
  
<head>
    <title>API - Big Cartel Help</title>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
 
    <link href="../../../css/all.css" rel="stylesheet" type="text/css" />
    <link rel="canonical" href="index.html">
  </head>

  <body class="developer">

    <div class="app">
      <div class="north-wrapper header--fixed">
        <header class="north">
          <div class="wrapper">
                <a href="../../../index.html" class="logo mark">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 62 78" enable-background="new 0 0 62 78" xml:space="preserve">
    <g class="mark"><path d="M30.8 42.5l0-3c0-2 1-4 2.8-5.2l25.1-16c1.8-1.2 2.8-3.1 2.8-5.1V0L30.8 19.6L0 0v41.4C0 52.7 5 62 13.2 67.2l17.5 11.2 l17.5-11.2c8.2-5.2 13.2-14.4 13.2-25.8V22.9L30.8 42.5z"/>
    </g>
  </svg>
</a>
                <ul class="breadcrumbs">
                    <li> <a href="../../index.html"><span>Back to </span>Developers</a></li>
                </ul>
            <a href="../../../contact/index.html" class="contact">
              <span class="text">Contact Us</span>
              <span class="icon">
                <img width="15" height="auto" alt="Contact Us" src="../../../images/layout/svg/envelope-icon.svg" />
              </span>
            </a>
          </div>
        </header>
      </div>

      <div class="content">
        
  <div class="support-main">
    <div class="support-body">
      <ul class="article-subnav">
        <li class="active"><a href="#intro">Intro</a></li>
        <li><a href="#authentication">Authentication</a></li>
        <li><a href="#versioning">Versioning</a></li>
        <li><a href="#identification">Identification</a></li>
        <li><a href="#rate-limiting">Rate limiting</a></li>
        <li><a href="#endpoints">Endpoints</a></li>
        <li><a href="#webhooks">Webhooks</a></li>
        <li><a href="#status-codes">Status codes</a></li>
        <li><a href="#pagination">Pagination</a></li>
        <li><a href="#images">Images</a></li>
      </ul>

      <article id="intro">
        <div class="article-content">
          <h1 class="title">API</h1>
          <div>
              <p>Our API makes it possible for anyone to make awesome things for Big Cartel’s massive community of artists. Integrate Big Cartel into an existing application or service, create an add-on to extend our core features, or code something up you’d like in your own shop. Whatever it is, we hope these tools allow you to build something great.</p>

<p><img alt="Big Cartel API" src="../../../images/developers/api/v1/api-cf4ae19d.png" /></p>

<blockquote>
  <p><strong>We’re in beta!</strong> Our API is early in its development, so not all features are available just yet, but all current features are documented, stable, and ready to roll. Join our <a href="http://news.bigcartel.com/h/y/297716CE01A49348">Developer Newsletter</a> and follow our <a href="changelog/index.html">API Changelog</a> to stay current on new features!</p>
</blockquote>

<h3 id="authentication">Authentication</h3>

<p>All API requests require you to have authentication to the account you’re trying to access. There are a couple of ways to do that, depending on the type of project you’re working on.</p>

<h4 id="public-projects">Public projects</h4>

<p>If you’d like to make an app or integration to be used by others, you’ll need to authenticate using <a href="oauth/index.html">OAuth</a>. This is currently <strong>invite-only during beta</strong>, but you can <a href="https://bigcartel.wufoo.com/forms/big-cartel-api-beta-application/">apply for an invite</a> by telling us a bit about who you are and what you’d like to build. If approved, we’ll provide you with a <code>client_id</code> and <code>client_secret</code>. <a href="oauth/index.html">Read our OAuth guide.</a></p>

<h4 id="personal-projects">Personal projects</h4>

<p>If you’d like to make a private integration just for yourself, you can do that without an invite by using HTTP Basic authentication with your store’s subdomain and password. Again though, this should only be for your own store, <strong>never ask anyone for their password!</strong></p>

<h3 id="versioning">Versioning</h3>

<p>The current version of the Big Cartel API is <strong>V1</strong>, and that version number is specified in all endpoints (e.g. <code>/v1/accounts/123</code>). Updates made to the V1 API will always be backwards-compatible, and listed with a datestamp in our <a href="changelog/index.html">API Changelog</a>.</p>

<blockquote>
  <p><strong>Looking for our old API?</strong> If you only need to get a few public details about a shop and its products, then you may want to check out our old <a href="../v0/index.html">read-only API.</a></p>
</blockquote>

<h3 id="identification">Identification</h3>

<p>You must include a <code>User-Agent</code> header with the name of your application and a contact URL or email where you can be reached. This allows us to warn you if you’re doing something wrong before you get blacklisted, or feature you if you’re doing something awesome.</p>

<pre class="highlight shell"><code>User-Agent: Example Inc. <span class="o">(</span>http://example.com/contact/<span class="o">)</span>
User-Agent: Example Project <span class="o">(</span>email@example.com<span class="o">)</span>
</code></pre>

<p>If you don’t supply this header, you will get a <code>400 Bad Request</code> response.</p>

<h3 id="rate-limiting">Rate limiting</h3>

<p>The API doesn’t currently limit the number of requests in a given time period, but rate limiting will be added in the future. For now, please use the <a href="#endpoints">endpoints</a> responsibly, and use <a href="#webhooks">webhooks</a> to know when an update happens instead of polling regularly.</p>

<h3 id="endpoints">Endpoints</h3>

<p>The API follows a <a href="http://en.wikipedia.org/wiki/Representational_state_transfer">REST</a> design to make our resource URLs simple and predictable. All responses use HTTP status codes and return <a href="http://en.wikipedia.org/wiki/JSON">JSON</a> following the <a href="http://jsonapi.org/">JSONAPI</a> standard.</p>

<p>All API endpoints begin with the root URL <code>https://api.bigcartel.com</code> and require an <code>Accept</code> header of <code>application/vnd.api+json</code>. When making a request that includes a JSON body, sending a <code>Content-type</code> header of <code>application/vnd.api+json</code> is required as well.</p>

<table>
  <thead>
    <tr>
      <th>Endpoint</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="account/index.html">Account</a></td>
      <td>Get info about an account.</td>
    </tr>
    <tr>
      <td><a href="artists/index.html">Artists</a></td>
      <td>Get info about and manage an account’s artists.</td>
    </tr>
    <tr>
      <td><a href="categories/index.html">Categories</a></td>
      <td>Get info about and manage an account’s categories.</td>
    </tr>
    <tr>
      <td><a href="countries/index.html">Countries</a></td>
      <td>Get a list of countries.</td>
    </tr>
    <tr>
      <td><a href="orders/index.html">Orders</a></td>
      <td>Get info about and update an account’s orders.</td>
    </tr>
    <tr>
      <td><a href="products/index.html">Products</a></td>
      <td>Get info about and manage an account’s products.</td>
    </tr>
  </tbody>
</table>

<h3 id="webhooks">Webhooks</h3>

<p>Our API makes use of <a href="webhooks/index.html">webhooks</a> to notify your application of changes to any of the account’s you care about. They’ll tell you what type of change has occurred, and will even include the updated object data so you don’t need to keep checking yourself. <a href="webhooks/index.html">Read our webhooks guide.</a></p>

<h3 id="status-codes">Status codes</h3>

<p>Our API returns <a href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes">HTTP status codes</a> for every request. Here are some examples of typical responses you can expect to receive:</p>

<table>
  <thead>
    <tr>
      <th>Code</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>200</td>
      <td>Everything worked as expected.</td>
    </tr>
    <tr>
      <td>400</td>
      <td>Bad Request - missing or invalid parameters.</td>
    </tr>
    <tr>
      <td>401</td>
      <td>Unauthorized - invalid API credentials.</td>
    </tr>
    <tr>
      <td>402</td>
      <td>Request Failed - valid parameters, but the request failed.</td>
    </tr>
    <tr>
      <td>404</td>
      <td>Not Found - requested item doesn’t exist.</td>
    </tr>
    <tr>
      <td>5xx</td>
      <td>Server Error - something is wrong on our end.</td>
    </tr>
  </tbody>
</table>

<h4 id="errors">Errors</h4>

<p>Some errors will return an additional <code>errors</code> object in the response with information in the following format:</p>

<pre class="highlight json"><code><span class="p">{</span><span class="w">
  </span><span class="nt">"errors"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="nt">"code"</span><span class="p">:</span><span class="w"> </span><span class="s2">"not-found"</span><span class="p">,</span><span class="w">
      </span><span class="nt">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"The item you requested was not found"</span><span class="p">,</span><span class="w">
      </span><span class="nt">"detail"</span><span class="p">:</span><span class="w"> </span><span class="s2">"We couldn't find that account"</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">]</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>

<h3 id="pagination">Pagination</h3>

<p>Requests that return multiple objects will be limited to 20 items by default. The documentation for each endpoint will tell you which objects support pagination, and you can specify further pages using <code>limit</code> and <code>offset</code> inside the <code>page</code> parameter like so:</p>

<pre class="highlight shell"><code>GET /v1/accounts/1/orders?page[limit]<span class="o">=</span>10&amp;page[offset]<span class="o">=</span>0
</code></pre>

<p>When an object supports pagination, a <code>links</code> object is returned that can contain <code>first</code>, <code>last</code>, <code>next</code>, and <code>prev</code> keys with links to their respective pages depending on what page of results is being returned. For example:</p>

<pre class="highlight json"><code><span class="p">{</span><span class="w">
  </span><span class="nt">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="err">...</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="nt">"links"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="nt">"next"</span><span class="p">:</span><span class="w"> </span><span class="s2">"http://api.bigcartel.com/v1/accounts/1/orders?page%5Blimit%5D=10&amp;page%5Boffset%5D=10"</span><span class="p">,</span><span class="w">
    </span><span class="nt">"last"</span><span class="p">:</span><span class="w"> </span><span class="s2">"http://api.bigcartel.com/v1/accounts/1/orders?page%5Blimit%5D=10&amp;page%5Boffset%5D=10"</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>

<p>Additionally, we’ll return a <code>meta</code> object with the total number of results available to be paginated through:</p>

<pre class="highlight json"><code><span class="p">{</span><span class="w">
  </span><span class="nt">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="err">...</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="nt">"meta"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="nt">"count"</span><span class="p">:</span><span class="w"> </span><span class="mi">60</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>

<h3 id="images">Images</h3>

<p>Any image URL attribute returned from an endpoint in our API will be in the following format:</p>

<pre class="highlight shell"><code>https://images.bigcartel.com/example_resource/12345/example.jpg?w<span class="o">=</span>1000&amp;h<span class="o">=</span>1000
</code></pre>

<p>However, you’d probably like a version optimized for your application. Fortunately, Big Cartel uses the awesome <a href="https://www.imgix.com/">imgix</a> service, and you can take advantage of their <a href="https://www.imgix.com/docs/reference">image API</a> to resize, crop, and stylize the image in any number of ways. For example:</p>

<pre class="highlight shell"><code>https://images.bigcartel.com/example_resource/12345/example.jpg?w<span class="o">=</span>500&amp;h<span class="o">=</span>250
https://images.bigcartel.com/example_resource/12345/example.jpg?h<span class="o">=</span>800&amp;fit<span class="o">=</span>max&amp;auto<span class="o">=</span>format
</code></pre>

<blockquote>
  <p><strong>Play nice!</strong> imgix’s image features unlock all sorts of exciting possibilities, but with great power comes great responsibility. Please don’t make excessive calls to imgix for the images in your app, or we’ll be forced to take your toys away. Thanks!</p>
</blockquote>

<p>If you’d like to get info or dimensions about the original image, just request <a href="https://www.imgix.com/docs/reference/format#param-fm">JSON format</a>:</p>

<pre class="highlight shell"><code>https://images.bigcartel.com/example_resource/12345/example.jpg?fm<span class="o">=</span>json
</code></pre>


          </div>
        </div>
      </article>
    </div>
  </div>

      </div>

      <footer>
        <ul>
          <li>&copy; 2015 <a href="#">Big Cartel, LLC</a></li>
          <li><a href="#">Legal</a></li>
        </ul>
      </footer>
    </div>

    <script src="../../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../../js/all.js" type="text/javascript"></script>


  </body>

</html>
