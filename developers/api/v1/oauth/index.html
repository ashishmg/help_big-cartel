<!doctype html>
<html lang="en">
  
<head>
    <title>OAuth - Big Cartel Help</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
 

    <link href="../../../../css/all.css" rel="stylesheet" type="text/css" />
    <link rel="canonical" href="index.html">
  </head>

  <body class="developer">

    <div class="app">
      <div class="north-wrapper header--fixed">
        <header class="north">
          <div class="wrapper">
                <a href="../../../../index.html" class="logo mark">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 62 78" enable-background="new 0 0 62 78" xml:space="preserve">
    <g class="mark"><path d="M30.8 42.5l0-3c0-2 1-4 2.8-5.2l25.1-16c1.8-1.2 2.8-3.1 2.8-5.1V0L30.8 19.6L0 0v41.4C0 52.7 5 62 13.2 67.2l17.5 11.2 l17.5-11.2c8.2-5.2 13.2-14.4 13.2-25.8V22.9L30.8 42.5z"/>
    </g>
  </svg>
</a>
                <ul class="breadcrumbs">
                    <li> <a href="../index.html"><span>Back to </span>API</a></li>
                </ul>
            <a href="../../../../contact/index.html" class="contact">
              <span class="text">Contact Us</span>
              <span class="icon">
                <img width="15" height="auto" alt="Contact Us" src="../../../../images/layout/svg/envelope-icon.svg" />
              </span>
            </a>
          </div>
        </header>
      </div>

      <div class="content">
        
  <div class="support-main">
    <div class="support-body">
      <ul class="article-subnav">
        <li class="active"><a href="#intro">Intro</a></li>
        <li><a href="#using-an-oauth-library">Using an OAuth library</a></li>
        <li><a href="#oauth-from-scratch">OAuth from scratch</a></li>
        <li><a href="#making-api-calls">Making API calls</a></li>
        <li><a href="#errors">Errors</a></li>
      </ul>

      <article id="intro">
        <div class="article-content">
          <h1 class="title">OAuth</h1>
          <div>
              <p>The Big Cartel API uses OAuth 2.0 to provide your application with access to an account without the need for sharing or storing passwords. To do this, you’ll redirect your user to Big Cartel, they’ll login and see a page explaining exactly who wants access and what they’ll have access to. After allowing or denying your request, they’ll be sent back to your site.</p>

<p><img alt="Authorization page" src="../../../../images/developers/api/v1/authorization-43ae3f4c.jpg" /></p>

<blockquote>
  <p><strong>Invite-only!</strong> To use OAuth you’ll first need to <a href="../index.html#authentication">request an invite</a>.</p>
</blockquote>

<h3 id="using-an-oauth-library">Using an OAuth library</h3>

<p>Since <a href="http://oauth.net/2/">OAuth</a> is a common and open protocol, there are already a number of projects that make it easy to use without much work on your part.</p>

<ol>
  <li>Grab an <a href="http://oauth.net/code/">OAuth library</a>.</li>
  <li>Configure it with your <code>client_id</code>, <code>client_secret</code>, and <code>redirect_uri</code>.</li>
  <li>Tell it to use <code>https://my.bigcartel.com/oauth/authorize</code> to request authorization and <code>https://api.bigcartel.com/oauth/token</code> to get access tokens.</li>
</ol>

<p>That’s it! You can now use your <code>access_token</code> to <a href="#making-api-calls">make API calls</a>.</p>

<h3 id="oauth-from-scratch">OAuth from scratch</h3>

<p>If you’re the type that likes to have full control over your code, and don’t mind reinventing a wheel now and then, you can create your own OAuth integration.</p>

<h4 id="redirect-the-user-to-big-cartel-to-request-access">1. Redirect the user to Big Cartel to request access</h4>

<p>To connect your application to a Big Cartel account, redirect them to our authorization page which will prompt them to give you access to their account.</p>

<pre class="highlight shell"><code>GET https://my.bigcartel.com/oauth/authorize
</code></pre>

<h5 id="parameters">Parameters</h5>

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>client_id</code></td>
      <td>string</td>
      <td>true</td>
      <td>Your unique client identifier from your application settings.</td>
    </tr>
    <tr>
      <td><code>response_type</code></td>
      <td>string</td>
      <td>false</td>
      <td>The only option right now is <strong>code</strong>.</td>
    </tr>
    <tr>
      <td><code>state</code></td>
      <td>string</td>
      <td>false</td>
      <td>An arbitrary value, sent back with each response (useful for identifying users on your end).</td>
    </tr>
    <tr>
      <td><code>redirect_uri</code></td>
      <td>string</td>
      <td>false</td>
      <td>URL where your users are sent after authorization.</td>
    </tr>
  </tbody>
</table>

<h4 id="big-cartel-redirects-back-to-your-site">2. Big Cartel redirects back to your site</h4>

<p>If the user accepts your request, they’ll be redirected to your <code>redirect_uri</code> with a temporary code in a <code>code</code> parameter as well as the state you may have provided in the previous step in a <code>state</code> parameter. If the states don’t match what you expect, the request has been created by a third party and the process should be aborted.</p>

<p>If the user denies your request, they’ll be redirected to your <code>redirect_uri</code> with the an <code>error</code> parameter containing the appropriate <a href="#errors">error code</a>.</p>

<h4 id="exchange-the-temporary-code-for-an-access-token">3. Exchange the temporary code for an access token</h4>

<p>You’ll now need to make a request to trade the <code>code</code> you received for an <code>access_token</code>.</p>

<pre class="highlight shell"><code>POST https://api.bigcartel.com/oauth/token
</code></pre>

<h5 id="parameters-1">Parameters</h5>

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>client_id</code></td>
      <td>string</td>
      <td>true</td>
      <td>Your unique client identifier from your application settings.</td>
    </tr>
    <tr>
      <td><code>client_secret</code></td>
      <td>string</td>
      <td>true</td>
      <td>Your unique and private client secret from your application settings.</td>
    </tr>
    <tr>
      <td><code>code</code></td>
      <td>string</td>
      <td>true</td>
      <td>The <code>code</code> parameter you received in the previous step.</td>
    </tr>
    <tr>
      <td><code>redirect_uri</code></td>
      <td>string</td>
      <td>false</td>
      <td>URL where your users are sent after authorization.</td>
    </tr>
  </tbody>
</table>

<h5 id="response">Response</h5>

<p>If successful, we’ll return a JSON response in the following format.</p>

<pre class="highlight json"><code><span class="p">{</span><span class="w">
  </span><span class="nt">"access_token"</span><span class="p">:</span><span class="s2">"YOUR-ACCESS-TOKEN"</span><span class="p">,</span><span class="w">
  </span><span class="nt">"token_type"</span><span class="p">:</span><span class="s2">"bearer"</span><span class="p">,</span><span class="w">
  </span><span class="nt">"account_id"</span><span class="p">:</span><span class="mi">12345</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>

<p>That’s it! You can now use your <code>access_token</code> to <a href="#making-api-calls">make API calls</a>.</p>

<h3 id="making-api-calls">Making API calls</h3>

<p>Once you’ve obtained an <code>access_token</code> for an account, you can use it in the <code>Authorization</code> header of all of your requests.</p>

<pre class="highlight shell"><code>Authorization: Bearer YOUR-ACCESS-TOKEN
</code></pre>

<p>For example, in curl you can set the <code>Authorization</code> header like this:</p>

<pre class="highlight shell"><code>curl -H <span class="s2">"Authorization: Bearer YOUR-ACCESS-TOKEN"</span> https://api.bigcartel.com/accounts/12345
</code></pre>

<h3 id="errors">Errors</h3>

<p>If at any point there are problems or invalid parameters in your request, we’ll send back an error code in the following format.</p>

<pre class="highlight json"><code><span class="p">{</span><span class="w">
  </span><span class="nt">"error"</span><span class="p">:</span><span class="s2">"client_secret_invalid"</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>

<p>Possible errors include:</p>

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>access_denied</code></td>
      <td>The user has denied access to their account.</td>
    </tr>
    <tr>
      <td><code>invalid_client_id</code></td>
      <td>The <code>client_id</code> you specified is invalid.</td>
    </tr>
    <tr>
      <td><code>invalid_client_secret</code></td>
      <td>The <code>client_secret</code> you specified is invalid.</td>
    </tr>
    <tr>
      <td><code>invalid_redirect_uri</code></td>
      <td>The <code>redirect_uri</code> you specified is invalid.</td>
    </tr>
    <tr>
      <td><code>invalid_response_type</code></td>
      <td>Your request’s response type is invalid.</td>
    </tr>
  </tbody>
</table>


          </div>
        </div>
      </article>
    </div>
  </div>

      </div>

      <footer>
        <ul>
          <li>&copy; 2015 <a href="#">Big Cartel, LLC</a></li>
          <li><a href="#">Legal</a></li>
        </ul>
      </footer>
    </div>

    <script src="../../../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../../../js/all.js" type="text/javascript"></script>

  </body>

</html>
